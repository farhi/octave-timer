# octave-timer
A Matlab-compatible timer class to execute periodic actions.

Use a timer to schedule one or multiple executions of tasks comprised of callback functions. If a timer is scheduled to execute multiple times, you can specify the time between executions.

Usage
=====
This class can be used to execute specified actions at regular time intervals.
Simply use:
```octave
obj = timer('TimerFcn', @(obj,evt)disp('Updated'), ...
        'Period', 2, 'ExecutionMode', 'fixedDelay');
start(obj);
```

All callback function should have syntax:
```octave
fun(obj,evt)
```
where `obj` is the current timer object, and `evt` is an event structure with `Type` as the type of callback (e.g. `TimerFcn`), and `Data` as the timer object. The callbacks can be given as a string, a function handle, or a cell `{ fun, arg1, arg1, ... }`.
In case the callback fails, the `ErrorFcn` callback is called.

Installation
============
Install `timer` manually with:

- launch octave
- pkg install https://gitlab.com/farhi/octave-timer/-/archive/0.1.1/octave-timer-0.1.1.tar.gz
- pkg load timer

Properties and Methods
======================

Here is a description of the class properties. The properties with a `*` are read-only.

Timer properties | Description
-----------------|--------------------------------------------------------
AveragePeriod*   | Average number of seconds between TimerFcn executions.
BusyMode*        | Action taken when TimerFcn executions are in progress.
ErrorFcn         | Callback function executed when an error occurs.
ExecutionMode    | Mode used to schedule timer events.
InstantPeriod*   | Elapsed time between the last two TimerFcn executions. 
Name             | Descriptive name of the timer object.
Period           | Seconds between TimerFcn executions.
Running*         | Timer object running status.
StartDelay       | Delay between START and the first scheduled TimerFcn execution.
StartFcn         | Callback function executed when timer object starts.
StopFcn          | Callback function executed after timer object stops.
Tag              | Label for object.
TasksExecuted*   | Number of TimerFcn executions that have occurred.
TasksToExecute   | Number of times to execute the TimerFcn callback.
time*            | Time when the timer was last updated.
TimerFcn         | Callback function executed when a timer event occurs.
UserData         | User data for timer object.
    
    
and for the Methods:
    
Timer method     | Description
-----------------|--------------------------------------------------------
@timer/timer     | Construct timer object.
delete           | Remove timer object from memory.
display          | Display method for timer objects.
start            | Start timer object running.
startat          | Start timer object running at a specified time.
stop             | Stop timer object running.
waitfor          | Wait for timer object to stop running.


Example
=======
You may test the `timer` class by running [timer_demo](https://raw.githubusercontent.com/farhi/octave-timer/master/example/timer_demo.m):
```octave
addpath /path/to/octave-timer
cd /path/to/octave-timer/example
timer_demo
```
which should return 'OK'. Look at its source to see how to use the class.

You may as well launch:
```octave
t=timer('TimerFcn','disp OK', 'Period',3, 'ExecutionMode','fixedDelay'); 
start(t);
```

Credits
=======
(c) E. Farhi / Synchrotron Soleil (2024), GPL 2. https://gitlab.com/farhi/octave-timer/
