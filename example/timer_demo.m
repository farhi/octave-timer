% demo for timer

% create object from a derived class
obj = timer('TimerFcn', @(o,e)disp(o.TasksExecuted), 'ExecutionMode','fixedDelay');

% set number of iterations
obj.TasksToExecute = 5;

% start
start(obj)

% wait until completed
waitfor(obj);

% test for nb of calls
if (obj.TasksExecuted == 5)
  disp('Test: OK') 
else 
  disp('Test: FAILED'); 
endif
